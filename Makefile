#################################################
# Development commands
#########################################

# start
start:
	docker-compose up

# stop 
stop: 
	docker-compose stop

# run test
test:
	docker-compose run --no-deps --rm server npm run test

# run test coverage
coverage:
	docker-compose run --no-deps --rm server npm run test:cov

# run test
lint:
	docker-compose run --no-deps --rm server npm run lint

# install dependencies
install:
	docker-compose run --no-deps --rm server npm install