import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleModule } from '@nestjs/schedule';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoggerMiddleware } from './common/middlewares/logger.middleware';
import { GlobalModule } from './common/modules/global.module';
import { Token } from './modules/auth/utils/token.entity';
import { News } from './modules/news/utils/news.entity';
import { User } from './modules/user/utils/user.entity';
import { TypeormConstantsService } from './common/utils/typeorm.constants';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: TypeormConstantsService.host,
      port: TypeormConstantsService.port,
      username: TypeormConstantsService.username,
      password: TypeormConstantsService.password,
      database: TypeormConstantsService.dbName,
      synchronize: true,
      // logging: true,
      entities: [User, Token, News],
    }),
    ScheduleModule.forRoot(),
    GlobalModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }
}
