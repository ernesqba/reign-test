import { Injectable, Logger } from '@nestjs/common';
import { NewsService } from '../../modules/news/news.service';
import { UserService } from '../../modules/user/user.service';
import * as bcrypt from 'bcrypt';
import { AuthConstantsService } from '../../modules/auth/utils/auth.constants';

@Injectable()
export class FirstStart {
  private readonly logger = new Logger('FirstStart');

  constructor(
    private readonly userService: UserService,
    private readonly newsService: NewsService,
    private readonly authConstantsService: AuthConstantsService,
  ) {}

  async isEmptyDb() {
    const users = await this.userService.getUsers();
    const news = await this.newsService.getNews({});
    return !(users.length || news.length);
  }

  async fillDb() {
    await this.userService.createUser({
      email: 'admin@admin.com',
      password: bcrypt.hashSync(
        'Admin123',
        this.authConstantsService.bcryptRounds,
      ),
    });
    await this.newsService.syncDb();
    this.logger.verbose('Filled Db for first time');
  }
}
