export class PaginationConstantsService {
  static get limit(): number {
    return +process.env['PAGINATION_LIMIT'] || 5;
  }
  static get offset(): number {
    return +process.env['PAGINATION_OFFSET'] || 0;
  }
}
