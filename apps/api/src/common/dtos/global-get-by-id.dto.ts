import { Type } from 'class-transformer';
import { IsPositive } from 'class-validator';

export class GlobalGetById {
  @IsPositive()
  @Type(() => Number)
  id: number;
}
