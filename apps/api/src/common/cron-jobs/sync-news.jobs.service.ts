import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { NewsService } from '../../modules/news/news.service';

@Injectable()
export class SyncNewsService {
  private readonly logger = new Logger('SyncNewsService');

  constructor(private readonly newsService: NewsService) {}

  @Cron('0 0 * * * *')
  handleCron() {
    this.logger.debug('Running Cron Job');
    this.newsService.syncDb();
  }
}
