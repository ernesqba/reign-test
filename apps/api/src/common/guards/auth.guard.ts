import {
  Injectable,
  CanActivate,
  ExecutionContext,
  Logger,
  UnauthorizedException,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { JwtService } from '@nestjs/jwt';

import { UserService } from '../../modules/user/user.service';
import { AuthDecryptedToken } from '../../modules/auth/dtos/auth-decrypted-token.dto';
import { IUserRequest } from '../../modules/auth/utils/iuser-request.interface';

@Injectable()
export class AuthGuard implements CanActivate {
  private readonly logger = new Logger('AuthGuard');

  constructor(
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
  ) {}

  private async validateRequest(request: IUserRequest) {
    let tokenData: AuthDecryptedToken;
    try {
      const token = request.headers['authorization'].split('Bearer ')[1];
      tokenData = this.jwtService.verify(token);
    } catch (error) {
      this.logger.error(error);
      throw new UnauthorizedException('Invalid token');
    }
    const user = await this.userService.getUserByEmail({
      email: tokenData.email,
    });
    request.user = user;
    return true;
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }
}
