import { Global, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';

import { AuthConstantsService } from '../../modules/auth/utils/auth.constants';
import { UserModule } from '../../modules/user/user.module';
import { JwtSign } from '../../modules/auth/utils/jwt.validator';
import { AuthGuard } from '../guards/auth.guard';
import { AuthModule } from '../../modules/auth/auth.module';
import { SyncNewsService } from '../cron-jobs/sync-news.jobs.service';
import { NewsModule } from '../../modules/news/news.module';
import { FirstStart } from '../utils/first-start.service';

@Global()
@Module({
  imports: [
    JwtModule.registerAsync({
      useFactory: (authConstanteService: AuthConstantsService) => {
        return {
          secret: authConstanteService.jwtSecret,
          signOptions: {
            expiresIn: authConstanteService.jwtExpirationTime,
          },
        };
      },
      inject: [AuthConstantsService],
    }),
    AuthModule,
    UserModule,
    NewsModule,
  ],
  controllers: [],
  providers: [
    JwtSign,
    AuthGuard,
    AuthConstantsService,
    SyncNewsService,
    FirstStart,
  ],
  exports: [
    JwtModule,
    AuthModule,
    UserModule,
    AuthConstantsService,
    FirstStart,
  ],
})
export class GlobalModule {}
