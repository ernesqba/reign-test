export class NewsCreateDto {
  title: string;
  author: string;
  tags: string;
  data: any;
  createdAt: Date;
}
