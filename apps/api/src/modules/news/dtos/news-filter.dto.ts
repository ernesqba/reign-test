import { IsIn, IsNumber, IsOptional, IsString } from 'class-validator';
import { Transform, Type } from 'class-transformer';
import { months } from '../../../common/utils/months.list';
import { PaginationConstantsService } from '../../../common/utils/pagination.constants';

export class NewsFilterDto {
  @IsString()
  @IsOptional()
  author?: string;

  @IsString()
  @IsOptional()
  tags?: string;

  @IsString()
  @IsOptional()
  @Transform(({ value }) => (value === 'null' ? null : value))
  title?: string;

  @IsString()
  @IsOptional()
  @IsIn(Object.keys(months))
  month?: string;

  @IsNumber()
  @IsOptional()
  @Type(() => Number)
  limit?: number = PaginationConstantsService.limit;

  @IsNumber()
  @IsOptional()
  @Type(() => Number)
  offset?: number = PaginationConstantsService.offset;
}
