import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpModule } from '@nestjs/axios';

import { NewsController } from './news.controller';
import { NewsService } from './news.service';
import { NewsRepository } from './utils/news.repository';

@Module({
  imports: [TypeOrmModule.forFeature([NewsRepository]), HttpModule],
  controllers: [NewsController],
  providers: [NewsService],
  exports: [NewsService],
})
export class NewsModule {}
