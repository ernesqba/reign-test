import {
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import { GlobalGetById } from '../../common/dtos/global-get-by-id.dto';
import { AuthGuard } from '../../common/guards/auth.guard';
import { TransformInterceptor } from '../../common/interceptors/transform.interceptor';
import { NewsFilterDto } from './dtos/news-filter.dto';
import { NewsService } from './news.service';
import { News } from './utils/news.entity';

@ApiTags('News')
@Controller('news')
export class NewsController {
  constructor(private readonly newsService: NewsService) {}

  @Post('sync')
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  syncDb(): Promise<void> {
    return this.newsService.syncDb();
  }

  @Get()
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @UseInterceptors(new TransformInterceptor(News))
  getNews(@Query() newsFilterDto: NewsFilterDto): Promise<News[]> {
    return this.newsService.getNews(newsFilterDto);
  }

  @Delete(':id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  delete(@Param() globalGetById: GlobalGetById): Promise<void> {
    return this.newsService.deleteNewsById(globalGetById);
  }
}
