import {
  DeleteResult,
  EntityRepository,
  getConnection,
  getManager,
  Repository,
} from 'typeorm';
import { GlobalGetById } from '../../../common/dtos/global-get-by-id.dto';
import { months } from '../../../common/utils/months.list';
import { NewsCreateDto } from '../dtos/news-create.dto';
import { NewsFilterDto } from '../dtos/news-filter.dto';

import { News } from './news.entity';

@EntityRepository(News)
export class NewsRepository extends Repository<News> {
  createNews(newsCreateDto: NewsCreateDto) {
    return this.save(newsCreateDto);
  }

  getNews(newsFilterDto: NewsFilterDto): Promise<News[]> {
    const query = getConnection().createQueryBuilder().select().from(News, 'n');

    if (newsFilterDto.author)
      query.andWhere('author LIKE :author', {
        author: '%' + newsFilterDto.author + '%',
      });
    if (newsFilterDto.tags)
      query.andWhere('tags LIKE :tags', {
        tags: '%' + newsFilterDto.tags + '%',
      });
    if (newsFilterDto.title)
      query.andWhere('title LIKE :title', {
        title: '%' + newsFilterDto.title + '%',
      });
    if (newsFilterDto.title === null) query.andWhere('title is null');
    if (newsFilterDto.month) {
      const date = new Date();
      const underDate = date.setFullYear(
        date.getFullYear(),
        months[newsFilterDto.month],
        1,
      );
      let overDate;
      if (months[newsFilterDto.month] === 11)
        overDate = date.setFullYear(date.getFullYear() + 1, 0, 1);
      else
        overDate = date.setFullYear(
          date.getFullYear(),
          months[newsFilterDto.month] + 1,
          1,
        );

      console.log(new Date(underDate), new Date(overDate));
      query
        .andWhere('created_at >= :underDate', {
          underDate: new Date(underDate),
        })
        .andWhere('created_at <= :overDate', { overDate: new Date(overDate) });
    }

    query.limit(newsFilterDto.limit);
    query.offset(newsFilterDto.offset);
    return query.execute();
  }

  deleteNewsById(globalGetById: GlobalGetById): Promise<DeleteResult> {
    return getConnection()
      .createQueryBuilder()
      .delete()
      .from(News)
      .where('id = :id', { id: globalGetById.id })
      .execute();
  }

  deleteTable() {
    return getManager().query(`DELETE FROM public."news" `);
  }
}
