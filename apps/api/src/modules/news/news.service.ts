import {
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { HttpService } from '@nestjs/axios';
import { AxiosResponse } from 'axios';

import { DatabaseException } from '../../common/filters/database-exception.filter';
import { News } from './utils/news.entity';
import { NewsRepository } from './utils/news.repository';
import { NewsCreateDto } from './dtos/news-create.dto';
import { GlobalGetById } from '../../common/dtos/global-get-by-id.dto';
import { NewsFilterDto } from './dtos/news-filter.dto';

@Injectable()
export class NewsService {
  private readonly logger = new Logger('NewsService');

  constructor(
    @InjectRepository(NewsRepository)
    private readonly newsRepository: NewsRepository,
    private readonly httpService: HttpService,
  ) {}

  findAll(): Promise<AxiosResponse<any>> {
    return this.httpService
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .toPromise();
  }

  async syncDb() {
    try {
      const data = (await this.findAll()).data.hits;
      await this.deleteTable();
      await Promise.all(
        data.map(async (item) => {
          const news: NewsCreateDto = {
            author: item.author,
            title: item.title,
            tags: item._tags.join(', '),
            createdAt: item.created_at,
            data: item,
          };
          await this.createNews(news);
        }),
      );
      this.logger.debug('Sync Db');
    } catch (error) {
      const textError = `Error inside syncDb logic: ${error}`;
      this.logger.error(textError);
      throw new InternalServerErrorException(textError);
    }
  }

  async createNews(newsCreateDto: NewsCreateDto): Promise<News> {
    try {
      return await this.newsRepository.createNews(newsCreateDto);
    } catch (error) {
      this.logger.error(`Error inside createNews logic: ${error}`);
      throw new DatabaseException();
    }
  }

  async getNews(newsFilterDto: NewsFilterDto): Promise<News[]> {
    try {
      return await this.newsRepository.getNews(newsFilterDto);
    } catch (error) {
      this.logger.error(`Error inside getNews logic: ${error}`);
      throw new DatabaseException();
    }
  }

  async deleteNewsById(globalGetById: GlobalGetById): Promise<void> {
    try {
      await this.newsRepository.deleteNewsById(globalGetById);
    } catch (error) {
      this.logger.error(`Error inside deleteNewsById logic: ${error}`);
      throw new DatabaseException();
    }
  }

  async deleteTable(): Promise<void> {
    try {
      return await this.newsRepository.deleteTable();
    } catch (error) {
      this.logger.error(`Error inside deleteTable logic: ${error}`);
      throw new DatabaseException();
    }
  }
}
