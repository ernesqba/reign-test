import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { AppModule } from '../../app.module';
import { DatabaseException } from '../../common/filters/database-exception.filter';
import { AuthRefreshTokenDto } from './dtos/auth-refresh-token.dto';
import { TokenService } from './token.service';
import { TokenTypesEnum } from './utils/token-types.enum';
import { TokenRepository } from './utils/token.repository';

describe('TokenService', () => {
  let tokenService: TokenService;
  let tokenRepository: TokenRepository;

  let createTokenMock;
  let getTokenAndUserMock;
  let deleteTokensForUserMock;
  let deleteTokensForEmailMock;
  let getTokenByTokenMock;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
      providers: [TokenService, TokenRepository],
    }).compile();

    tokenService = module.get<TokenService>(TokenService);
    tokenRepository = module.get<TokenRepository>(TokenRepository);

    createTokenMock = jest.spyOn(tokenRepository, 'createToken');
    getTokenAndUserMock = jest.spyOn(tokenRepository, 'getTokenAndUser');
    deleteTokensForUserMock = jest.spyOn(
      tokenRepository,
      'deleteTokensForUser',
    );
    deleteTokensForEmailMock = jest.spyOn(
      tokenRepository,
      'deleteTokensForEmail',
    );
    getTokenByTokenMock = jest.spyOn(tokenRepository, 'getTokenByToken');
  });

  describe('createToken logic', () => {
    let response;
    const token = { token: 'token', type: TokenTypesEnum.AUTH, userId: 1 };

    describe('success case', () => {
      beforeAll(async () => {
        jest.resetAllMocks();
        createTokenMock.mockImplementation(() => Promise.resolve(token));

        response = await tokenService.createToken(token);
      });

      it('should return a token', () => {
        expect(response).toBe(token);
      });

      it('should call the mocks', () => {
        expect(createTokenMock).toBeCalledTimes(1);
        expect(getTokenAndUserMock).toBeCalledTimes(0);
        expect(deleteTokensForUserMock).toBeCalledTimes(0);
        expect(deleteTokensForEmailMock).toBeCalledTimes(0);
      });
    });

    describe('database error', () => {
      beforeAll(async () => {
        jest.resetAllMocks();
        createTokenMock.mockImplementation(() => Promise.reject('Error'));

        try {
          await tokenService.createToken(token);
        } catch (error) {
          response = error;
        }
      });

      it('should return a DatabaseException error', () => {
        expect(response).toBeInstanceOf(DatabaseException);
      });

      it('should call the mocks', () => {
        expect(createTokenMock).toBeCalledTimes(1);
        expect(getTokenAndUserMock).toBeCalledTimes(0);
        expect(deleteTokensForUserMock).toBeCalledTimes(0);
        expect(deleteTokensForEmailMock).toBeCalledTimes(0);
      });
    });
  });

  describe('getTokenByToken logic', () => {
    let response;
    const token = 'token';
    const fullToken = {
      token,
    };

    describe('success case', () => {
      beforeAll(async () => {
        jest.resetAllMocks();
        getTokenByTokenMock.mockImplementation(() =>
          Promise.resolve(fullToken),
        );

        response = await tokenService.getTokenByToken(token);
      });

      it('should return a token', () => {
        expect(response).toBe(fullToken);
      });

      it('should call the mocks', () => {
        expect(getTokenByTokenMock).toBeCalledTimes(1);
      });
    });

    describe('not found case', () => {
      beforeAll(async () => {
        jest.resetAllMocks();
        getTokenByTokenMock.mockImplementation(() => Promise.resolve(null));

        try {
          await tokenService.getTokenByToken(token);
        } catch (error) {
          response = error;
        }
      });

      it('should return a NotFoundException error', () => {
        expect(response).toBeInstanceOf(NotFoundException);
      });

      it('should call the mocks', () => {
        expect(getTokenByTokenMock).toBeCalledTimes(1);
      });
    });

    describe('database error', () => {
      beforeAll(async () => {
        jest.resetAllMocks();
        getTokenByTokenMock.mockImplementation(() => Promise.reject('Error'));

        try {
          await tokenService.getTokenByToken(token);
        } catch (error) {
          response = error;
        }
      });

      it('should return a DatabaseException error', () => {
        expect(response).toBeInstanceOf(DatabaseException);
      });

      it('should call the mocks', () => {
        expect(getTokenByTokenMock).toBeCalledTimes(1);
      });
    });
  });

  describe('getTokenAndUser logic', () => {
    let response;
    const token = 'token';
    const authRefreshToken: AuthRefreshTokenDto = {
      refreshToken: token,
      email: 'a@a.a',
    };
    const tokenUser = {
      token,
      User: {
        id: 1,
      },
    };

    describe('success case', () => {
      beforeAll(async () => {
        jest.resetAllMocks();
        getTokenAndUserMock.mockImplementation(() =>
          Promise.resolve([tokenUser]),
        );

        response = await tokenService.getTokenAndUser(authRefreshToken);
      });

      it('should return a token with its user', () => {
        expect(response).toBe(tokenUser);
      });

      it('should call the mocks', () => {
        expect(createTokenMock).toBeCalledTimes(0);
        expect(getTokenAndUserMock).toBeCalledTimes(1);
        expect(deleteTokensForUserMock).toBeCalledTimes(0);
        expect(deleteTokensForEmailMock).toBeCalledTimes(0);
      });
    });

    describe('database error', () => {
      beforeAll(async () => {
        jest.resetAllMocks();
        getTokenAndUserMock.mockImplementation(() => Promise.reject('Error'));

        try {
          await tokenService.getTokenAndUser(authRefreshToken);
        } catch (error) {
          response = error;
        }
      });

      it('should return a DatabaseException error', () => {
        expect(response).toBeInstanceOf(DatabaseException);
      });

      it('should call the mocks', () => {
        expect(createTokenMock).toBeCalledTimes(0);
        expect(getTokenAndUserMock).toBeCalledTimes(1);
        expect(deleteTokensForUserMock).toBeCalledTimes(0);
        expect(deleteTokensForEmailMock).toBeCalledTimes(0);
      });
    });
  });

  describe('deleteTokensForUser logic', () => {
    let response;
    const UserId = 1;
    const tokenTypes = [TokenTypesEnum.AUTH, TokenTypesEnum.REFRESH];
    const answer = 'Ok';

    describe('success case', () => {
      beforeAll(async () => {
        jest.resetAllMocks();
        deleteTokensForUserMock.mockImplementation(() =>
          Promise.resolve(answer),
        );

        response = await tokenService.deleteTokensForUser(UserId, tokenTypes);
      });

      it('should return an answer', () => {
        expect(response).toBe(answer);
      });

      it('should call the mocks', () => {
        expect(createTokenMock).toBeCalledTimes(0);
        expect(getTokenAndUserMock).toBeCalledTimes(0);
        expect(deleteTokensForUserMock).toBeCalledTimes(1);
        expect(deleteTokensForEmailMock).toBeCalledTimes(0);
      });
    });

    describe('database error', () => {
      beforeAll(async () => {
        jest.resetAllMocks();
        deleteTokensForUserMock.mockImplementation(() =>
          Promise.reject('Error'),
        );

        try {
          await tokenService.deleteTokensForUser(UserId, tokenTypes);
        } catch (error) {
          response = error;
        }
      });

      it('should return a DatabaseException error', () => {
        expect(response).toBeInstanceOf(DatabaseException);
      });

      it('should call the mocks', () => {
        expect(createTokenMock).toBeCalledTimes(0);
        expect(getTokenAndUserMock).toBeCalledTimes(0);
        expect(deleteTokensForUserMock).toBeCalledTimes(1);
        expect(deleteTokensForEmailMock).toBeCalledTimes(0);
      });
    });
  });

  describe('deleteTokensForEmail logic', () => {
    let response;
    const email = 'a@a.a';
    const tokenTypes = [TokenTypesEnum.AUTH, TokenTypesEnum.REFRESH];
    const answer = 'Ok';

    describe('success case', () => {
      beforeAll(async () => {
        jest.resetAllMocks();
        deleteTokensForEmailMock.mockImplementation(() =>
          Promise.resolve(answer),
        );

        response = await tokenService.deleteTokensForEmail(email, tokenTypes);
      });

      it('should return an answer', () => {
        expect(response).toBe(answer);
      });

      it('should call the mocks', () => {
        expect(createTokenMock).toBeCalledTimes(0);
        expect(getTokenAndUserMock).toBeCalledTimes(0);
        expect(deleteTokensForUserMock).toBeCalledTimes(0);
        expect(deleteTokensForEmailMock).toBeCalledTimes(1);
      });
    });

    describe('database error', () => {
      beforeAll(async () => {
        jest.resetAllMocks();
        deleteTokensForEmailMock.mockImplementation(() =>
          Promise.reject('Error'),
        );

        try {
          await tokenService.deleteTokensForEmail(email, tokenTypes);
        } catch (error) {
          response = error;
        }
      });

      it('should return a DatabaseException error', () => {
        expect(response).toBeInstanceOf(DatabaseException);
      });

      it('should call the mocks', () => {
        expect(createTokenMock).toBeCalledTimes(0);
        expect(getTokenAndUserMock).toBeCalledTimes(0);
        expect(deleteTokensForUserMock).toBeCalledTimes(0);
        expect(deleteTokensForEmailMock).toBeCalledTimes(1);
      });
    });
  });
});
