import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { DeleteResult } from 'typeorm';

import { DatabaseException } from '.././../common/filters/database-exception.filter';
import { User } from '../user/utils/user.entity';
import { AuthRefreshTokenDto } from './dtos/auth-refresh-token.dto';
import { TokenInDto } from './dtos/token-in.dto';
import { TokenTypesEnum } from './utils/token-types.enum';
import { Token } from './utils/token.entity';
import { TokenRepository } from './utils/token.repository';

@Injectable()
export class TokenService {
  private readonly logger = new Logger('TokenService');

  constructor(private readonly tokenRepository: TokenRepository) {}

  async createToken(tokenIn: TokenInDto): Promise<Token> {
    try {
      const tokenOut = await this.tokenRepository.createToken(tokenIn);
      return tokenOut;
    } catch (error) {
      this.logger.error(`Error inside createToken logic: ${error}`);
      throw new DatabaseException();
    }
  }

  async getTokenByToken(token: string): Promise<Token> {
    let Token;
    try {
      Token = await this.tokenRepository.getTokenByToken(token);
    } catch (error) {
      this.logger.error(`Error inside getTokenByToken logic: ${error}`);
      throw new DatabaseException();
    }
    if (!Token) throw new NotFoundException('Token not found by token');
    return Token;
  }

  async getTokenAndUser(
    authRefreshTokenDto: AuthRefreshTokenDto,
  ): Promise<Token & User> {
    try {
      const tokenUser = (
        await this.tokenRepository.getTokenAndUser(authRefreshTokenDto)
      )[0];
      tokenUser['userId'] = tokenUser['user_id'];
      delete tokenUser['user_id'];
      return tokenUser;
    } catch (error) {
      this.logger.error(`Error inside getTokenAndUser logic: ${error}`);
      throw new DatabaseException();
    }
  }

  async deleteTokensForUser(
    UserId: number,
    types: TokenTypesEnum[],
  ): Promise<DeleteResult> {
    try {
      return await this.tokenRepository.deleteTokensForUser(UserId, types);
    } catch (error) {
      this.logger.error(`Error inside deleteTokensForUser logic: ${error}`);
      throw new DatabaseException();
    }
  }

  async deleteTokensForEmail(
    email: string,
    types: TokenTypesEnum[],
  ): Promise<DeleteResult> {
    try {
      const a = await this.tokenRepository.deleteTokensForEmail(email, types);
      return a;
    } catch (error) {
      this.logger.error(`Error inside deleteTokensForEmail logic: ${error}`);
      throw new DatabaseException();
    }
  }
}
