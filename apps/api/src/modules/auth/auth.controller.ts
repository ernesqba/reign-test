import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TransformInterceptor } from '../../common/interceptors/transform.interceptor';

import { AuthService } from './auth.service';
import { AuthCredentialsDto } from './dtos/auth-credentials.dto';
import { AuthRefreshTokenDto } from './dtos/auth-refresh-token.dto';
import { AuthTokensDto } from './dtos/auth-tokens.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/login')
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(new TransformInterceptor(AuthTokensDto))
  login(
    @Body() authCredentialsDto: AuthCredentialsDto,
  ): Promise<AuthTokensDto> {
    return this.authService.login(authCredentialsDto);
  }

  @Post('/refresh')
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(new TransformInterceptor(AuthTokensDto))
  refresh(
    @Body() authRefreshTokenDto: AuthRefreshTokenDto,
  ): Promise<AuthTokensDto> {
    return this.authService.refresh(authRefreshTokenDto);
  }

  @Post('logout')
  @HttpCode(HttpStatus.NO_CONTENT)
  logout(@Body() authRefreshTokenDto: AuthRefreshTokenDto): Promise<void> {
    return this.authService.logout(authRefreshTokenDto);
  }
}
