import { NotFoundException, UnauthorizedException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { AppModule } from '../../app.module';
import { UserService } from '../user/user.service';
import { AuthService } from './auth.service';
import { AuthCredentialsDto } from './dtos/auth-credentials.dto';
import { AuthRefreshTokenDto } from './dtos/auth-refresh-token.dto';
import { TokenService } from './token.service';
import { JwtVtFgPayload } from './utils/jwt-vt-payload.interface';
import { TokenRepository } from './utils/token.repository';

describe('AuthService', () => {
  let authService: AuthService;
  let userService: UserService;
  let tokenService: TokenService;

  let getUserByEmailMock;
  let checkUserExistByEmailMock;
  let createTokenMock;
  let getTokenAndUserMock;
  let deleteTokensForUserMock;
  let deleteTokensForEmailMock;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
      providers: [AuthService, TokenService, TokenRepository],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    userService = module.get<UserService>(UserService);
    tokenService = module.get<TokenService>(TokenService);

    getUserByEmailMock = jest.spyOn(userService, 'getUserByEmail');
    checkUserExistByEmailMock = jest.spyOn(
      userService,
      'checkUserExistByEmail',
    );
    createTokenMock = jest.spyOn(tokenService, 'createToken');
    getTokenAndUserMock = jest.spyOn(tokenService, 'getTokenAndUser');
    deleteTokensForUserMock = jest.spyOn(tokenService, 'deleteTokensForUser');
    deleteTokensForEmailMock = jest.spyOn(tokenService, 'deleteTokensForEmail');
  });

  describe('generate verify token logic', () => {
    let response;

    describe('success case', () => {
      const payload: JwtVtFgPayload = {
        email: 'a@a.com',
        now: Date.now(),
      };

      beforeAll(async () => {
        response = await authService.generateVerifyToken(payload);
      });

      it('should return void', () => {
        expect(typeof response.verifyToken).toBe('string');
      });
    });
  });

  describe('login logic', () => {
    let response;

    describe('success case', () => {
      const email = 'a@a.a';
      const user = {
        name: 'User1',
        email,
        status: 'enabled',
        password:
          '$2b$10$WmsIpnXZmI3YoZrO0/3sWO3FMvtMuGdocz3xoeTc9scNGa6UqhE7a',
      };
      const authCredentials: AuthCredentialsDto = {
        email,
        password: 'Admin123',
      };

      beforeAll(async () => {
        jest.resetAllMocks();
        getUserByEmailMock.mockImplementation(() => Promise.resolve(user));
        deleteTokensForUserMock.mockImplementation(() => Promise.resolve());
        createTokenMock.mockImplementation(() => Promise.resolve());
        createTokenMock.mockImplementation(() => Promise.resolve());

        response = await authService.login(authCredentials);
      });

      it('should return the auth tokens', () => {
        expect(Object.keys(response)).toStrictEqual([
          'accessToken',
          'refreshToken',
        ]);
      });

      it('should call the mocks', () => {
        expect(getUserByEmailMock).toBeCalledTimes(1);
        expect(createTokenMock).toBeCalledTimes(2);
        expect(getTokenAndUserMock).toBeCalledTimes(0);
        expect(deleteTokensForUserMock).toBeCalledTimes(1);
        expect(deleteTokensForEmailMock).toBeCalledTimes(0);
      });
    });

    describe('invalid credentials', () => {
      const email = 'a@a.a';
      const user = {
        name: 'User1',
        email,
        status: 'enabled',
        password:
          '$2b$10$WmsIpnXZmI3YoZrO0/3sWO3FMvtMuGdocz3xoeTc9scNGa6UqhE7a',
      };
      const authCredentials: AuthCredentialsDto = {
        email,
        password: 'Admin122',
      };

      beforeAll(async () => {
        jest.resetAllMocks();
        getUserByEmailMock.mockImplementation(() => Promise.resolve(user));

        try {
          await authService.login(authCredentials);
        } catch (error) {
          response = error;
        }
      });

      it('should return a UnauthorizedException error', () => {
        expect(response).toBeInstanceOf(UnauthorizedException);
      });

      it('should call the mocks', () => {
        expect(getUserByEmailMock).toBeCalledTimes(1);
        expect(createTokenMock).toBeCalledTimes(0);
        expect(getTokenAndUserMock).toBeCalledTimes(0);
        expect(deleteTokensForUserMock).toBeCalledTimes(0);
        expect(deleteTokensForEmailMock).toBeCalledTimes(0);
      });
    });
  });

  describe('refresh logic', () => {
    let response;

    describe('success case', () => {
      const email = 'a@a.a';
      const tokenUser = {
        token: 'oldRefreshToken',
        User: {
          name: 'User1',
          email,
          status: 'enabled',
          password:
            '$2b$10$WmsIpnXZmI3YoZrO0/3sWO3FMvtMuGdocz3xoeTc9scNGa6UqhE7a',
        },
      };
      const authRefreshToken: AuthRefreshTokenDto = {
        email,
        refreshToken: 'oldRefreshToken',
      };

      beforeAll(async () => {
        jest.resetAllMocks();
        checkUserExistByEmailMock.mockImplementation(() =>
          Promise.resolve(true),
        );
        getTokenAndUserMock.mockImplementation(() =>
          Promise.resolve(tokenUser),
        );
        deleteTokensForUserMock.mockImplementation(() => Promise.resolve());
        createTokenMock.mockImplementation(() => Promise.resolve());
        createTokenMock.mockImplementation(() => Promise.resolve());

        response = await authService.refresh(authRefreshToken);
      });

      it('should return the auth tokens', () => {
        expect(Object.keys(response)).toStrictEqual([
          'accessToken',
          'refreshToken',
        ]);
      });

      it('should call the mocks', () => {
        expect(checkUserExistByEmailMock).toBeCalledTimes(1);
        expect(getTokenAndUserMock).toBeCalledTimes(1);
        expect(deleteTokensForUserMock).toBeCalledTimes(1);
        expect(createTokenMock).toBeCalledTimes(2);
      });
    });

    describe('user not found case', () => {
      const email = 'a@a.a';
      const authRefreshToken: AuthRefreshTokenDto = {
        email,
        refreshToken: 'oldRefreshToken',
      };

      beforeAll(async () => {
        jest.resetAllMocks();
        checkUserExistByEmailMock.mockImplementation(() =>
          Promise.resolve(false),
        );

        try {
          response = await authService.refresh(authRefreshToken);
        } catch (error) {
          response = error;
        }
      });

      it('should return a NotFoundException error', () => {
        expect(response).toBeInstanceOf(NotFoundException);
      });

      it('should call the mocks', () => {
        expect(checkUserExistByEmailMock).toBeCalledTimes(1);
        expect(getTokenAndUserMock).toBeCalledTimes(0);
        expect(deleteTokensForEmailMock).toBeCalledTimes(0);
        expect(createTokenMock).toBeCalledTimes(0);
      });
    });

    describe('invalid credentials', () => {
      const email = 'a@a.a';
      const authRefreshToken: AuthRefreshTokenDto = {
        email,
        refreshToken: 'oldRefreshToken',
      };

      beforeAll(async () => {
        jest.resetAllMocks();
        checkUserExistByEmailMock.mockImplementation(() =>
          Promise.resolve(true),
        );
        getTokenAndUserMock.mockImplementation(() => Promise.resolve());
        deleteTokensForEmailMock.mockImplementation(() => Promise.resolve());

        try {
          await authService.refresh(authRefreshToken);
        } catch (error) {
          response = error;
        }
      });

      it('should return a UnauthorizedException error', () => {
        expect(response).toBeInstanceOf(UnauthorizedException);
      });

      it('should call the mocks', () => {
        expect(checkUserExistByEmailMock).toBeCalledTimes(1);
        expect(getTokenAndUserMock).toBeCalledTimes(1);
        expect(deleteTokensForEmailMock).toBeCalledTimes(1);
        expect(createTokenMock).toBeCalledTimes(0);
      });
    });
  });

  describe('logout logic', () => {
    let response;

    describe('success case', () => {
      const email = 'a@a.a';
      const tokenUser = {
        token: 'oldRefreshToken',
        User: {
          name: 'User1',
          email,
          status: 'enabled',
          password:
            '$2b$10$WmsIpnXZmI3YoZrO0/3sWO3FMvtMuGdocz3xoeTc9scNGa6UqhE7a',
        },
      };
      const authRefreshToken: AuthRefreshTokenDto = {
        email,
        refreshToken: 'oldRefreshToken',
      };

      beforeAll(async () => {
        jest.resetAllMocks();
        checkUserExistByEmailMock.mockImplementation(() =>
          Promise.resolve(true),
        );
        getTokenAndUserMock.mockImplementation(() =>
          Promise.resolve(tokenUser),
        );
        deleteTokensForEmailMock.mockImplementation(() => Promise.resolve());

        response = await authService.logout(authRefreshToken);
      });

      it('should return void auth tokens', () => {
        expect(response).toBeUndefined();
      });

      it('should call the mocks', () => {
        expect(checkUserExistByEmailMock).toBeCalledTimes(1);
        expect(getTokenAndUserMock).toBeCalledTimes(1);
        expect(deleteTokensForEmailMock).toBeCalledTimes(1);
      });
    });

    describe('user not found case', () => {
      const email = 'a@a.a';
      const authRefreshToken: AuthRefreshTokenDto = {
        email,
        refreshToken: 'oldRefreshToken',
      };

      beforeAll(async () => {
        jest.resetAllMocks();
        checkUserExistByEmailMock.mockImplementation(() =>
          Promise.resolve(false),
        );

        try {
          response = await authService.logout(authRefreshToken);
        } catch (error) {
          response = error;
        }
      });

      it('should return a NotFoundException error', () => {
        expect(response).toBeInstanceOf(NotFoundException);
      });

      it('should call the mocks', () => {
        expect(checkUserExistByEmailMock).toBeCalledTimes(1);
        expect(getTokenAndUserMock).toBeCalledTimes(0);
        expect(deleteTokensForEmailMock).toBeCalledTimes(0);
      });
    });

    describe('invalid credentials case', () => {
      const email = 'a@a.a';
      const authRefreshToken: AuthRefreshTokenDto = {
        email,
        refreshToken: 'oldRefreshToken',
      };

      beforeAll(async () => {
        jest.resetAllMocks();
        checkUserExistByEmailMock.mockImplementation(() =>
          Promise.resolve(true),
        );
        getTokenAndUserMock.mockImplementation(() => Promise.resolve(null));

        try {
          response = await authService.logout(authRefreshToken);
        } catch (error) {
          response = error;
        }
      });

      it('should return a UnauthorizedException error', () => {
        expect(response).toBeInstanceOf(UnauthorizedException);
      });

      it('should call the mocks', () => {
        expect(checkUserExistByEmailMock).toBeCalledTimes(1);
        expect(getTokenAndUserMock).toBeCalledTimes(1);
        expect(deleteTokensForEmailMock).toBeCalledTimes(0);
      });
    });
  });
});
