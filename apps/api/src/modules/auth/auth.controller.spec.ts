import { Test, TestingModule } from '@nestjs/testing';

import { AppModule } from '../../app.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { AuthCredentialsDto } from './dtos/auth-credentials.dto';
import { AuthRefreshTokenDto } from './dtos/auth-refresh-token.dto';
import { AuthTokensDto } from './dtos/auth-tokens.dto';
import { TokenService } from './token.service';
import { TokenRepository } from './utils/token.repository';

describe('AuthController', () => {
  let authController: AuthController;
  let authService: AuthService;

  let loginMock;
  let refreshMock;
  let logoutMock;

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
      controllers: [AuthController],
      providers: [AuthService, TokenService, TokenRepository],
    }).compile();

    authController = app.get<AuthController>(AuthController);
    authService = app.get<AuthService>(AuthService);

    loginMock = jest.spyOn(authService, 'login');
    refreshMock = jest.spyOn(authService, 'refresh');
    logoutMock = jest.spyOn(authService, 'logout');
  });

  describe('login logic', () => {
    let response;
    const authCredentials: AuthCredentialsDto = {
      email: 'a@a.a',
      password: '123456',
    };

    const authToken: AuthTokensDto = {
      accessToken: 'accessToken',
      refreshToken: 'refreshToken',
    };

    beforeAll(async () => {
      loginMock.mockReset();
      loginMock.mockImplementation(() => Promise.resolve(authToken));

      response = await authController.login(authCredentials);
    });

    it('should return the auth tokens', () => {
      expect(response).toBe(authToken);
    });

    it('should call the mocks', () => {
      expect(loginMock).toBeCalledTimes(1);
    });
  });

  describe('refresh logic', () => {
    let response;
    const authRefreshToken: AuthRefreshTokenDto = {
      email: 'a@a.a',
      refreshToken: 'oldRefreshToken',
    };

    const authToken: AuthTokensDto = {
      accessToken: 'accessToken',
      refreshToken: 'refreshToken',
    };

    beforeAll(async () => {
      refreshMock.mockReset();
      refreshMock.mockImplementation(() => Promise.resolve(authToken));

      response = await authController.refresh(authRefreshToken);
    });

    it('should return the auth tokens', () => {
      expect(response).toBe(authToken);
    });

    it('should call the mocks', () => {
      expect(loginMock).toBeCalledTimes(1);
    });
  });

  describe('logout logic', () => {
    let response;
    const authRefreshToken: AuthRefreshTokenDto = {
      email: 'a@a.a',
      refreshToken: 'oldRefreshToken',
    };

    beforeAll(async () => {
      logoutMock.mockReset();
      logoutMock.mockImplementation(() => Promise.resolve(undefined));

      response = await authController.logout(authRefreshToken);
    });

    it('should return void', () => {
      expect(response).toBe(undefined);
    });

    it('should call the mocks', () => {
      expect(logoutMock).toBeCalledTimes(1);
    });
  });
});
