export interface JwtPayload {
  email: string;
  expiresIn: number;
}
