import {
  DeleteResult,
  EntityRepository,
  getConnection,
  getManager,
  Repository,
} from 'typeorm';

import { User } from '../../user/utils/user.entity';
import { AuthRefreshTokenDto } from '../dtos/auth-refresh-token.dto';
import { TokenInDto } from '../dtos/token-in.dto';
import { TokenTypesEnum } from './token-types.enum';
import { Token } from './token.entity';

@EntityRepository(User)
export class TokenRepository extends Repository<Token> {
  createToken(token: TokenInDto): Promise<Token> {
    return getConnection()
      .createQueryBuilder()
      .insert()
      .into(Token)
      .values({
        token: token.token,
        type: token.type,
        expiresIn: token.expiresIn,
        userId: token.userId,
      })
      .returning('*')
      .execute()
      .then((data) => data.raw[0]);
  }

  getTokenByToken(token: string): Promise<Token> {
    return this.findOne({ token });
  }

  getTokenAndUser(
    authRefreshTokenDto: AuthRefreshTokenDto,
  ): Promise<Token & User> {
    return getConnection()
      .createQueryBuilder()
      .innerJoin('user', 'user', 'user.id = token.user_id')
      .select()
      .from(Token, 'token')
      .where('token = :token', { token: authRefreshTokenDto.refreshToken })
      .execute();
  }

  deleteTokensForUser(
    userId: number,
    types: TokenTypesEnum[],
  ): Promise<DeleteResult> {
    return getConnection()
      .createQueryBuilder()
      .delete()
      .from(Token)
      .where('type IN (:...types)', { types })
      .andWhere('userId = :userId', { userId })
      .execute();
  }

  deleteTokensForEmail(
    email: string,
    types: TokenTypesEnum[],
  ): Promise<DeleteResult> {
    return getManager().query(
      `DELETE FROM public."token" 
      WHERE public."token".id IN (
        SELECT t.id 
        FROM public."token" t 
        INNER JOIN public."user" u 
        ON u.id = t.user_id  
        WHERE t."type" IN ('${types.join("', '")}') 
        AND u.email = '${email}'
      );`,
    );
  }
}
