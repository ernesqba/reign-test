import { Injectable } from '@nestjs/common';

@Injectable()
export class AuthConstantsService {
  get jwtSecret(): string {
    return process.env['JWT_SECRET'] || 'supersecret';
  }
  get jwtExpirationTime(): number {
    return +process.env['JWT_EXPIRATION_TIME'] || 300;
  }
  get bcryptRounds(): number {
    return +process.env['BCRYPT_ROUNDS'] || 10;
  }
}
