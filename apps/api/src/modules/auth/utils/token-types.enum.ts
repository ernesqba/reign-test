export enum TokenTypesEnum {
  AUTH = 'auth',
  REFRESH = 'refresh',
  FORGOT = 'forgot',
}
