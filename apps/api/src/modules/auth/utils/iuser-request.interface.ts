import { User } from '../../user/utils/user.entity';

export interface IUserRequest extends Request {
  user: User;
}
