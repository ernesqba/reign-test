import { createParamDecorator, ExecutionContext } from '@nestjs/common';

import { User } from '../../user/utils/user.entity';
import { IUserRequest } from './iuser-request.interface';

export const GetUser = createParamDecorator(
  (_data, ctx: ExecutionContext): User => {
    const req = ctx.switchToHttp().getRequest<IUserRequest>();
    return req.user;
  },
);
