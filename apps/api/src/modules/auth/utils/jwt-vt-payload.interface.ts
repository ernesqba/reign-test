export interface JwtVtFgPayload {
  email: string;
  now: number;
}
