import { Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import {
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ name: 'ValidJwtSign', async: true })
@Injectable()
export class JwtSign implements ValidatorConstraintInterface {
  private readonly logger = new Logger('JwtSign');

  constructor(private readonly jwtService: JwtService) {}

  async validate(value) {
    try {
      await this.jwtService.verify(value, { ignoreExpiration: true });
    } catch (error) {
      this.logger.warn(`Error inside validate jwt sign: ${error}`);
      return false;
    }
    return true;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  defaultMessage(args: ValidationArguments) {
    return `No valid JWT`;
  }
}
