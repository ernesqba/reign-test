import {
  Injectable,
  Logger,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';

import { UserService } from '../user/user.service';
import { AuthCredentialsDto } from './dtos/auth-credentials.dto';
import { AuthTokensDto } from './dtos/auth-tokens.dto';
import { JwtPayload } from './utils/jwt-payload.interface';
import { JwtRtPayload } from './utils/jwt-rt-payload.interface';
import { AuthConstantsService } from './utils/auth.constants';
import { AuthRefreshTokenDto } from './dtos/auth-refresh-token.dto';
import { TokenService } from './token.service';
import { JwtVtFgPayload } from './utils/jwt-vt-payload.interface';
import { TokenTypesEnum } from './utils/token-types.enum';

@Injectable()
export class AuthService {
  private readonly logger = new Logger('AuthService');

  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    private readonly tokenService: TokenService,
    private readonly authConstantsService: AuthConstantsService,
  ) {}

  async generateAccessToken(
    payload: JwtPayload,
  ): Promise<{ accessToken: string }> {
    const accessToken = await this.jwtService.sign(payload, {
      expiresIn: payload.expiresIn,
    });
    return { accessToken };
  }

  async generateRefreshToken(
    payload: JwtRtPayload,
  ): Promise<{ refreshToken: string }> {
    const refreshToken = await this.jwtService.sign(payload);
    return { refreshToken };
  }

  async generateVerifyToken(
    payload: JwtVtFgPayload,
  ): Promise<{ verifyToken: string }> {
    const verifyToken = await this.jwtService.sign(payload);
    return { verifyToken };
  }

  async login(authCredentialsDto: AuthCredentialsDto): Promise<AuthTokensDto> {
    const { email, password } = authCredentialsDto;
    const user = await this.userService.getUserByEmail({ email });
    if (await bcrypt.compare(password, user.password)) {
      const { accessToken } = await this.generateAccessToken({
        email,
        expiresIn: this.authConstantsService.jwtExpirationTime,
      });
      const { refreshToken } = await this.generateRefreshToken({
        email,
      });
      await this.tokenService.deleteTokensForUser(user.id, [
        TokenTypesEnum.AUTH,
        TokenTypesEnum.REFRESH,
      ]);
      await this.tokenService.createToken({
        token: accessToken,
        type: TokenTypesEnum.AUTH,
        expiresIn: this.authConstantsService.jwtExpirationTime,
        userId: user.id,
      });
      await this.tokenService.createToken({
        token: refreshToken,
        type: TokenTypesEnum.REFRESH,
        userId: user.id,
      });

      return { accessToken, refreshToken };
    }
    const errorText = 'Invalid credentials';
    this.logger.log(errorText);
    throw new UnauthorizedException(errorText);
  }

  async refresh(
    authRefreshTokenDto: AuthRefreshTokenDto,
  ): Promise<AuthTokensDto> {
    const { email } = authRefreshTokenDto;
    if (!(await this.userService.checkUserExistByEmail({ email }))) {
      const errorText = 'User not found';
      this.logger.log(errorText);
      throw new NotFoundException(errorText);
    }
    const tokenUser = await this.tokenService.getTokenAndUser(
      authRefreshTokenDto,
    );
    if (!tokenUser) {
      await this.tokenService.deleteTokensForEmail(email, [
        TokenTypesEnum.AUTH,
        TokenTypesEnum.REFRESH,
      ]);
      const errorText =
        'Invalid credentials (user not found or refresh token is expired or revoked)';
      this.logger.log(errorText);
      throw new UnauthorizedException(errorText);
    }

    const { accessToken } = await this.generateAccessToken({
      email,
      expiresIn: this.authConstantsService.jwtExpirationTime,
    });
    const { refreshToken } = await this.generateRefreshToken({
      email,
    });
    await this.tokenService.deleteTokensForUser(tokenUser.userId, [
      TokenTypesEnum.AUTH,
      TokenTypesEnum.REFRESH,
    ]);
    await this.tokenService.createToken({
      token: accessToken,
      type: TokenTypesEnum.AUTH,
      expiresIn: this.authConstantsService.jwtExpirationTime,
      userId: tokenUser.userId,
    });
    await this.tokenService.createToken({
      token: refreshToken,
      type: TokenTypesEnum.REFRESH,
      userId: tokenUser.userId,
    });

    return { accessToken, refreshToken };
  }

  async logout(authRefreshTokenDto: AuthRefreshTokenDto): Promise<void> {
    const { email } = authRefreshTokenDto;
    if (!(await this.userService.checkUserExistByEmail({ email }))) {
      const errorText = 'User not found';
      this.logger.log(errorText);
      throw new NotFoundException(errorText);
    }
    const tokenUser = await this.tokenService.getTokenAndUser(
      authRefreshTokenDto,
    );
    if (!tokenUser) {
      const errorText =
        'Invalid credentials (refresh token is expired or revoked)';
      this.logger.log(errorText);
      throw new UnauthorizedException(errorText);
    }
    await this.tokenService.deleteTokensForEmail(email, [
      TokenTypesEnum.AUTH,
      TokenTypesEnum.REFRESH,
    ]);
  }
}
