import { IsEmail, IsString, Validate } from 'class-validator';

import { JwtSign } from '../utils/jwt.validator';
export class AuthRefreshTokenDto {
  @IsString()
  @Validate(JwtSign)
  refreshToken: string;

  @IsEmail()
  email: string;
}
