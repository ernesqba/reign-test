import { TokenTypesEnum } from '../utils/token-types.enum';

export class TokenInDto {
  token: string;
  type: TokenTypesEnum;
  expiresIn?: number;
  userId: number;
}
