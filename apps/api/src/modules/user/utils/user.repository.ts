import { EntityRepository, Repository } from 'typeorm';
import { UserCreateDto } from '../dtos/user-create.dto';

import { UserGetByEmail } from '../dtos/user-get-by-email.dto';
import { User } from './user.entity';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  createUser(userCreateDto: UserCreateDto): Promise<User> {
    return this.save(userCreateDto);
  }

  getUsers(): Promise<User[]> {
    return this.find();
  }

  getUserByEmail(userGetByEmail: UserGetByEmail): Promise<User> {
    return this.findOne({
      email: userGetByEmail.email,
    });
  }
}
