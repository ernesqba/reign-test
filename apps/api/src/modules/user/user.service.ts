import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { DatabaseException } from '../../common/filters/database-exception.filter';
import { UserCreateDto } from './dtos/user-create.dto';
import { UserGetByEmail } from './dtos/user-get-by-email.dto';
import { User } from './utils/user.entity';
import { UserRepository } from './utils/user.repository';

@Injectable()
export class UserService {
  private readonly logger = new Logger('UserService');

  constructor(
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository,
  ) {}

  async createUser(userCreateDto: UserCreateDto): Promise<User> {
    try {
      return await this.userRepository.createUser(userCreateDto);
    } catch (error) {
      this.logger.error(`Error inside createUser logic: ${error}`);
      throw new DatabaseException();
    }
  }

  async getUsers(): Promise<User[]> {
    try {
      return await this.userRepository.getUsers();
    } catch (error) {
      this.logger.error(`Error inside getUsers logic: ${error}`);
      throw new DatabaseException();
    }
  }

  async getUserByEmail(userGetByEmail: UserGetByEmail): Promise<User> {
    let user;
    try {
      user = await this.userRepository.getUserByEmail(userGetByEmail);
    } catch (error) {
      this.logger.error(`Error inside getUserByEmail logic: ${error}`);
      throw new DatabaseException();
    }
    if (!user) throw new NotFoundException('User not found by email');
    return user;
  }

  async checkUserExistByEmail(
    userGetByEmail: UserGetByEmail,
  ): Promise<boolean> {
    let user;
    try {
      user = await this.userRepository.getUserByEmail(userGetByEmail);
    } catch (error) {
      this.logger.error(`Error inside checkUserExistByEmail logic: ${error}`);
      throw new DatabaseException();
    }
    if (!user) return false;
    return true;
  }
}
