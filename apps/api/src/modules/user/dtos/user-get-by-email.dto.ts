import { IsEmail } from 'class-validator';

export class UserGetByEmail {
  @IsEmail()
  email: string;
}
