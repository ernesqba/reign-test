import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {
  SwaggerModule,
  DocumentBuilder,
  SwaggerDocumentOptions,
} from '@nestjs/swagger';
import { Logger, ValidationPipe } from '@nestjs/common';
import { useContainer } from 'class-validator';
import { FirstStart } from './common/utils/first-start.service';

async function bootstrap() {
  const logger = new Logger('bootstrap');
  const app = await NestFactory.create(AppModule);
  const port = process.env.PORT;
  app.enableCors();

  app.useGlobalPipes(
    new ValidationPipe({
      disableErrorMessages: false,
      transform: true,
    }),
  );

  const firstStart = app.get<FirstStart>(FirstStart);
  try {
    if (await firstStart.isEmptyDb()) await firstStart.fillDb();
  } catch {
    logger.error('Error when try to fill DB in the first time');
  }

  const swaggerConfig = new DocumentBuilder()
    .setTitle('Reign Test API')
    .setDescription('Reign Test Rest Api')
    .setVersion('0.1')
    .addBearerAuth()
    .build();

  const swaggerOptions: SwaggerDocumentOptions = {};
  const document = SwaggerModule.createDocument(
    app,
    swaggerConfig,
    swaggerOptions,
  );
  SwaggerModule.setup('api/docs', app, document);

  useContainer(app.select(AppModule), { fallbackOnErrors: true });

  await app.listen(port);
  logger.log(`Application listening on port ${port}`);
}
bootstrap();
